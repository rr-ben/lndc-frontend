import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { RoutingPaths } from 'lndc-frontend-utils';

const routes: Routes = [
    {
        path: RoutingPaths.INTERNAL,
        loadChildren: () => import('./internal/internal.module').then(m => m.InternalModule)
    },
    {
        path: RoutingPaths.AUTH,
        loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
    },
    {
        path: RoutingPaths.EMPTY,
        loadChildren: () => import('./external/external.module').then(m => m.ExternalModule)
    },
    { path:  '**', redirectTo: RoutingPaths.EMPTY }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
