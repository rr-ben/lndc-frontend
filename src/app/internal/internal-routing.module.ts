import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InternalComponent } from './internal.component';
import { RoutingPaths } from 'lndc-frontend-utils';

const routes: Routes = [
    {
        path: RoutingPaths.EMPTY,
        component: InternalComponent
    },
    { path:  '**', redirectTo: RoutingPaths.EMPTY }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InternalRoutingModule { }
