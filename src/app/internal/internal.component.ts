import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-internal',
  templateUrl: './internal.template.html',
  styleUrls: ['./internal.style.less']
})
export class InternalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
