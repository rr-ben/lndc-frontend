import { Injectable } from '@angular/core';

import { NetworkService } from 'eto-portal-frontend-utils';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private get btoDbProxyUrl(): string {
        return 'https://bto-eto-ng.peppercon.de:5085/';
    }

    constructor(private readonly networkService: NetworkService) { }
    
    login(username: string, password: string, rememberMe: boolean) {
        
    }

    register(username: string, password: string) {
        
    }
}
