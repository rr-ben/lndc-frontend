import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

import { ExternalRoutingModule } from './external-routing.module';
import { ExternalComponent } from './external.component';

@NgModule({
  declarations: [
    ExternalComponent,
   ],
  imports: [
    CommonModule,
    ExternalRoutingModule,
    MatToolbarModule,
    MatIconModule
  ]
})
export class ExternalModule { }
