import { Component, OnInit } from '@angular/core';

import { RoutingPaths } from 'lndc-frontend-utils';

@Component({
    selector: 'app-external',
    templateUrl: './external.template.html',
    styleUrls: ['./external.style.less']
})
export class ExternalComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
    }

    get registrationPath() {
        return RoutingPaths.AUTH + "/" + RoutingPaths.REGISTRATION
    }
}
