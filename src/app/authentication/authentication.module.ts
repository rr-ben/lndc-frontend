import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

import { ServiceModule } from 'lndc-frontend-utils';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';

@NgModule({
  declarations: [
    AuthenticationComponent,
   ],
  imports: [
    CommonModule,
    MatCardModule,
    ServiceModule,
    AuthenticationRoutingModule
  ]
})
export class AuthenticationModule { }
