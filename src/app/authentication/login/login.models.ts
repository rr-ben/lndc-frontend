export interface LoginRequest {
    username: string;
    password: string;
    shouldRememberUser?: boolean;
}

export interface LoginResponse {
    uuid: string;
    expiration: number;
}