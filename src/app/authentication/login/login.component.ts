import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { ValidatorService } from 'lndc-frontend-utils';
import { RoutingPaths } from 'lndc-frontend-utils';

import { AuthenticationService } from '../authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.template.html',
    styleUrls: ['./login.style.less']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;

    usernameAttr = "username";
    passwordAttr = "password";
    rememberMeAttr = "rememberMe";

    constructor(private readonly formBuilder: FormBuilder, private readonly validator: ValidatorService, private readonly authService: AuthenticationService) { }

    ngOnInit(): void {
        const loginFormGroup = { }
        loginFormGroup[this.usernameAttr]   = ['', this.validator.required()]
        loginFormGroup[this.passwordAttr]   = ['', this.validator.required()]
        loginFormGroup[this.rememberMeAttr] = ['', this.validator.required()]
        this.loginForm = this.formBuilder.group(loginFormGroup);
    }

    getRequiredError(attrName: string) {
        return this.getControlErrors(attrName, ValidatorService.requiredErrorAttr);
    }

    getValidEmailError(attrName: string) {
        return this.getControlErrors(attrName, ValidatorService.validEmailErrorAttr);
    }

    onLogin() {
        const username: string = this.loginForm.get(this.usernameAttr).value;
        const password: string = this.loginForm.get(this.passwordAttr).value;
        const rememberMe: boolean = this.loginForm.get(this.rememberMeAttr).value;
        this.loginForm.get(this.passwordAttr).reset();

        this.authService.login(username, password, rememberMe).subscribe(
            res => {
                console.log("response")
                console.log(res);
            },
            err => {
                console.log("error")
                console.error(err);
            }
        )
    }

    get registrationPath() {
        return RoutingPaths.AUTH + "/" + RoutingPaths.REGISTRATION
    }

    private getControlErrors(attrName: string, errAttrName: string) {
        const controls = this.loginForm.controls[attrName];
        if (controls) {
            const errors = controls['errors']
            if (errors) {
                return errors[errAttrName]
            }
        }

        return undefined
    }
}
