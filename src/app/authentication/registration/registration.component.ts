import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { ValidatorService } from 'lndc-frontend-utils';
import { RoutingPaths } from 'lndc-frontend-utils';

import { AuthenticationService } from '../authentication.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.template.html',
    styleUrls: ['./registration.style.less']
})
export class RegistrationComponent implements OnInit {

    registrationForm: FormGroup;
    emailAttr = "email";
    usernameAttr = "username";
    passwordAttr = "password";

    constructor(private readonly formBuilder: FormBuilder, private readonly validator: ValidatorService, private readonly authService: AuthenticationService) { }

    ngOnInit(): void {
        const registrationFormGroup = { }
        registrationFormGroup[this.usernameAttr] = ['', this.validator.required()]
        registrationFormGroup[this.emailAttr]    = ['', Validators.compose([this.validator.required(), this.validator.validMail()])]
        registrationFormGroup[this.passwordAttr] = ['', this.validator.required()]
        this.registrationForm = this.formBuilder.group(registrationFormGroup);
    }

    getRequiredError(attrName: string) {
        return this.getControlErrors(attrName, ValidatorService.requiredErrorAttr);
    }

    getValidEmailError(attrName: string) {
        return this.getControlErrors(attrName, ValidatorService.validEmailErrorAttr);
    }

    onRegister() {
        const email: string = this.registrationForm.get(this.emailAttr).value;
        const username: string = this.registrationForm.get(this.usernameAttr).value;
        const password: string = this.registrationForm.get(this.passwordAttr).value;
        this.registrationForm.get(this.passwordAttr).reset();

        this.authService.register(email, username, password).subscribe(
            res => {
                console.log("response")
                console.log(res);
            },
            err => {
                console.log("error")
                console.error(err);
            }
        )
    }

    get loginPath() {
        return RoutingPaths.AUTH + "/" + RoutingPaths.LOGIN
    }

    private getControlErrors(attrName: string, errAttrName: string) {
        const controls = this.registrationForm.controls[attrName];
        if (controls) {
            const errors = controls['errors']
            if (errors) {
                return errors[errAttrName]
            }
        }

        return undefined
    }
}
