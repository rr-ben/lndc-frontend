import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationComponent } from './registration.component';
import { RoutingPaths } from 'lndc-frontend-utils';

const routes: Routes = [
    {
        path: RoutingPaths.EMPTY,
        component: RegistrationComponent,
    },

    { path:  '**', redirectTo: RoutingPaths.EMPTY }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RegistrationRoutingModule { }
