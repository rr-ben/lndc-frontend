import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationComponent } from './authentication.component';
import { RoutingPaths } from 'lndc-frontend-utils';

const routes: Routes = [
    {
        path: RoutingPaths.EMPTY,
        component: AuthenticationComponent,
        children: [
            {
                path: RoutingPaths.REGISTRATION,
                loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule)
            },
            {
                path: RoutingPaths.LOGIN,
                loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
            }
        ]
    },
    { path:  '**', redirectTo:  RoutingPaths.EMPTY }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthenticationRoutingModule {
    
//    Check whether login or registration url is used
}
