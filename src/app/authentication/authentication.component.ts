import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-authentication',
    templateUrl: './authentication.template.html',
    styleUrls: ['./authentication.style.less']
})
export class AuthenticationComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
    }
}
