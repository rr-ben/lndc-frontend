import { Injectable } from '@angular/core';

import { RegistrationRequest } from './registration/registration.models';
import { LoginRequest } from './login/login.models';
import { NetworkService } from 'lndc-frontend-utils';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private get btoDbProxyUrl(): string {
        return 'http://localhost:8080/api/';
    }

    constructor(private readonly networkService: NetworkService) { }

    login(username: string, password: string, shouldRememberUser: boolean) {
        const url = this.btoDbProxyUrl + "default_login"
        const request: LoginRequest = { username: username, password: password, shouldRememberUser: shouldRememberUser };
        return this.networkService.post(url, request);
    }

    register(email: string, username: string, password: string) {
        const url = this.btoDbProxyUrl + "register"
        const request: RegistrationRequest = { email: email, username: username, password: password };
        return this.networkService.post(url, request);
    }
}
