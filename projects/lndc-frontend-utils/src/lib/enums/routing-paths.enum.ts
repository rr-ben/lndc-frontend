export enum RoutingPaths {
    INTERNAL = 'internal',
    AUTH = 'auth',
    LOGIN = 'login',
    REGISTRATION = 'registration',
    EMPTY = ''
}
