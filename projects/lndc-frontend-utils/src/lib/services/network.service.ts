import { Injectable }           from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders }           from '@angular/common/http';

import { Observable, Subject, forkJoin } from 'rxjs';

@Injectable()
export class NetworkService {

    constructor(private readonly httpClient: HttpClient) { }

    post(url, body): Observable<any> {
        const httpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
console.log(httpHeaders)
        return this.httpClient.post(url, body, { headers: httpHeaders });
    }

    get(url, params): Observable<any> {
        if (params == undefined) {
            params = [ ];
        }

        const httpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
        const httpParams = new HttpParams();
        for (let key of params) {
            const value = params[key];
            httpParams.set(key, value);
        }

        return this.httpClient.get(url, { headers: httpHeaders });
    }
}
