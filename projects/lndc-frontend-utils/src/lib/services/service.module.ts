import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule }                  from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { NetworkService }   from './network.service';
import { ValidatorService }   from './validator.service';

@NgModule({
  imports: [ CommonModule, HttpClientModule ],
  providers: [ NetworkService, ValidatorService ]
})
export class ServiceModule { }
