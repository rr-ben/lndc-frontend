import { Injectable } from '@angular/core';

import { AbstractControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ValidatorService {

    static requiredErrorAttr = "required";
    static validEmailErrorAttr = "email";

    required(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors|null => {
            const error = { };
            const validation = Validators.required.call(null, control);
            if (validation != null) {
                error[ValidatorService.requiredErrorAttr] = "This field is required";
                return error
            }

            return null;
        }
    }

    validMail(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors|null => {
            const error = { };
            const raritanEmailValidationPattern = "^[a-zA-Z]+\.[a-zA-Z]+\@[a-zA-Z]+\.[a-zA-Z]+$";
            const validation = Validators.pattern(raritanEmailValidationPattern).call(null, control);
            if (validation != null) {
                error[ValidatorService.validEmailErrorAttr] = "This is no valid E-Mail address";
                return error;
            }

            return null;
        }
    }
}